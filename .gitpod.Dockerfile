FROM gitpod/workspace-full

# https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/prepare.md#ubuntu

RUN sudo apt-get update \
    && sudo apt-get install -y \
        software-properties-common \
        postgresql \
        postgresql-contrib \
        libpq-dev \
        redis-server \
        libicu-dev \
        cmake \
        g++ \
        g++-8 \
        libre2-dev \
        libkrb5-dev \
        libsqlite3-dev \
        ed \
        pkg-config \
        graphicsmagick \
        runit \
        libimage-exiftool-perl \
        rsync \
        libssl-dev \
    && sudo apt-get clean \
    && sudo rm -rf /var/cache/apt/* /var/lib/apt/lists/* /tmp/*

RUN sudo curl https://dl.min.io/server/minio/release/linux-amd64/minio --output /usr/local/bin/minio && \
    sudo chmod +x /usr/local/bin/minio

# scrips/lint-doc.sh dependency
RUN brew install vale
